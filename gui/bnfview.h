/*
 *  BNF Parser (bnfparse)
 *  Copyright © 2023 Andy Warner
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#pragma once

#include "wx/docview.h"
#include "wx/textctrl.h"
#include <ostream>

// Forward reference
class MainPanel;


class BnfView : public wxView {
public:
  BnfView();

  virtual bool OnCreate(wxDocument *doc, long flags) override;
  virtual void OnDraw(wxDC *dc) override;
  virtual bool OnClose(bool deleteWindow = true) override;

  std::ostream& GetLog() const;
  wxButton* GetButton(int id) const;
  wxTextCtrl* GetText(const std::string& name) const;

private:
  void OnCopy(wxCommandEvent& WXUNUSED(event)) { GetText("bnf")->Copy(); }
  void OnPaste(wxCommandEvent& WXUNUSED(event)) { GetText("bnf")->Paste(); }
  void OnSelectAll(wxCommandEvent& WXUNUSED(event)) { GetText("bnf")->SelectAll(); }

  MainPanel* panel_;

  wxDECLARE_EVENT_TABLE();
  wxDECLARE_DYNAMIC_CLASS(BnfView);
};
