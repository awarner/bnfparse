/*
 *  BNF Parser (bnfparse)
 *  Copyright © 2023 Andy Warner
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#pragma once

#include "wx/wx.h"


class BnfGui : public wxApp {
public:
  BnfGui();
  bool OnInit() override;
  int OnExit() override;

  wxFrame* GetChildFrame(wxView* view);
  void SetMenubar(bool opened);

private:
  wxMenuBar* CreateMenubar();
  void OnAbout(wxCommandEvent& event);

  wxFrame* frame_;
  wxMenu* fileMenuInit_;
  wxMenu* fileMenuOpened_;
  wxMenu* fileMruMenu_;

  wxDECLARE_NO_COPY_CLASS(BnfGui);
};
 
wxDECLARE_APP(BnfGui);
