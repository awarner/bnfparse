/*
 *  BNF Parser (bnfparse)
 *  Copyright © 2023 Andy Warner
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "bnfdoc.h"
#include "bnflanguage.h"
#include "bnfview.h"
#include "custom_events.h"
#include "parse.h"
#include "wx/file.h"
#include "wx/wfstream.h"
using bnf::Parse;
using std::endl;

wxIMPLEMENT_DYNAMIC_CLASS(BnfDocument, wxDocument);

wxBEGIN_EVENT_TABLE(BnfDocument, wxDocument)
    EVT_BUTTON(ID_BROWSE, BnfDocument::OnBrowseSample)
    EVT_BUTTON(ID_PARSE, BnfDocument::OnParse)
wxEND_EVENT_TABLE()

bool BnfDocument::OnCreate(const wxString& path, long flags) {
  if (!wxDocument::OnCreate(path, flags)) {
    return false;
  }
  // subscribe to changes in the text control to update the document state
  // when it's modified
  GetTextCtrl()->Bind(wxEVT_TEXT, &BnfDocument::OnTextChange, this);

  return true;
}

wxButton* BnfDocument::GetButton(int id) {
  wxView* view = GetFirstView();
  return view ? wxStaticCast(view, BnfView)->GetButton(id) : nullptr;
}

wxTextCtrl* BnfDocument::GetTextCtrl(const std::string& name) const {
  wxView* view = GetFirstView();
  return view ? wxStaticCast(view, BnfView)->GetText(name) : nullptr;
}

bool BnfDocument::IsModified() const {
  wxTextCtrl* wnd = GetTextCtrl();
  return wxDocument::IsModified() || (wnd && wnd->IsModified());
}

void BnfDocument::Modify(bool modified) {
  wxDocument::Modify(modified);

  wxTextCtrl* wnd = GetTextCtrl();
  if (wnd && !modified) {
    wnd->DiscardEdits();
  }
  wnd = GetTextCtrl("root");
  if (wnd && !modified) {
    wnd->DiscardEdits();
  }
  wnd = GetTextCtrl("sample");
  if (wnd && !modified) {
    wnd->DiscardEdits();
  }
  wnd = GetTextCtrl("sample_filename");
  if (wnd && !modified) {
    wnd->DiscardEdits();
  }
  wnd = GetTextCtrl("output");
  if (wnd && !modified) {
    wnd->DiscardEdits();
  }
}

bool BnfDocument::DoSaveDocument(const wxString& filename) {
  // Manually save the file
  wxFile bnfFile(filename);
  if (!bnfFile.IsOpened()) {
    return false;
  }
  bnfFile.Write("// Bnfparse 0.3\n// Root: ");
  auto txt = GetTextCtrl("root");
  auto root = txt->GetValue();
  bnfFile.Write(root);
  bnfFile.Write("\n// Sample: ");
  txt = GetTextCtrl("sample_filename");
  auto sample = txt->GetValue();
  bnfFile.Write(sample + "\n\n");
  txt = GetTextCtrl();
  auto bnf = txt->GetValue();
  bnfFile.Write(bnf);
  return true;
}

bool BnfDocument::DoOpenDocument(const wxString& filename) {
  wxFile bnfFile(filename);
  if (!bnfFile.IsOpened()) {
    return false;
  }
  wxString bnftext;
  if (!bnfFile.ReadAll(&bnftext)) {
    return false;
  }
  ssize_t pos = 0;
  if (bnftext.substr(pos, 12) != "// Bnfparse ") {
    return false;
  }
  pos += 12;
  if (bnftext.substr(pos, 3) != "0.3") {
    return false;
  }
  pos += 3;
  auto nxtl = bnftext.substr(pos).find_first_not_of(" \n\r");
  if (nxtl == std::string::npos) {
    return false;
  }
  pos += nxtl;
  if (bnftext.substr(pos, 9) == "// Root: ") {
    pos += 9;
    auto str = bnftext.substr(pos);
    auto rsz = str.find_first_of(" \n\r");
    auto root = str.substr(0, rsz);
    auto txt = GetTextCtrl("root");
    txt->SetValue(root);
    pos += rsz;
    nxtl = bnftext.substr(pos).find_first_not_of(" \n\r");
    if (nxtl == std::string::npos) {
      return false;
    }
    pos += nxtl;
  }
  if (bnftext.substr(pos, 11) == "// Sample: ") {
    pos += 11;
    auto str = bnftext.substr(pos);
    auto rsz = str.find_first_of(" \n\r");
    auto sample = str.substr(0, rsz);
    auto txtfn = GetTextCtrl("sample_filename");
    txtfn->SetValue(sample);
    pos += rsz;
    auto txt = GetTextCtrl("sample");
    auto sampsz = sample.find_last_of("/\\:");
    auto sample_base = (sampsz == std::string::npos) ? sample : sample.substr(sampsz);
    auto dir_end = filename.find_last_of("/\\:");
    auto dirname = dir_end == std::string::npos ? "" : filename.substr(0, dir_end + 1);
    auto fullname = dirname + sample_base;
    txt->LoadFile(fullname);
    nxtl = bnftext.substr(pos).find_first_not_of(" \n\r");
    if (nxtl == std::string::npos) {
      return false;
    }
    pos += nxtl;
  }

  GetTextCtrl()->SetValue(bnftext.substr(pos));
  // we're not modified by the user yet
  Modify(false);

  return true;
}

void BnfDocument::OnBrowseSample(wxCommandEvent& event) {
  wxView* v = GetFirstView();
  BnfView* view = wxStaticCast(v, BnfView);
  auto& log = view->GetLog();
  log << "OnBrowseSample called" << endl;

  wxFileDialog openFileDialog(view->GetText("bnf"), _("Open sample file"), "", "",
                              "sample files (*.*)|*.*", wxFD_OPEN|wxFD_FILE_MUST_EXIST);
  if (openFileDialog.ShowModal() == wxID_CANCEL) {
    return;     // the user changed idea...
  }

  // proceed loading the file chosen by the user;
  // this can be done with e.g. wxWidgets input streams:
  auto path = openFileDialog.GetPath();
  wxFileInputStream in_stream(path);
  if (!in_stream.IsOk()) {
    log << "Cannot open file " << path << endl;
    return;
  }
  auto txt = GetTextCtrl("sample");
  txt->LoadFile(path);
  txt = GetTextCtrl("sample_filename");
  txt->SetValue(path);
}

void BnfDocument::OnParse(wxCommandEvent& event) {
  wxView* view = GetFirstView();
  auto& log = wxStaticCast(view, BnfView)->GetLog();
  log << "OnParse called" << endl;

  bnf::BnfLanguage lang(log);
  auto txt = GetTextCtrl("output");
  txt->Clear();
  txt = GetTextCtrl("bnf");
  auto rules = txt->GetValue().ToStdString();
  if (!lang.ParsePatterns(rules)) {
    log << "ERROR: Could not parse pattern text (rules)" << endl;
    return;
  }

  bnf::INT iSize;
  bnf::INT iVariant[10];
  std::deque<std::string> tokens;

  txt = GetTextCtrl("root");
  auto root = txt->GetValue().ToStdString();
  txt = GetTextCtrl("sample");
  auto input = txt->GetValue().ToStdString();

  //
  // Parse the main text ...
  //
  if (Parse::Lookup(root) == nullptr) {
    log << "Root pattern does not exist" << endl;
    // Get rid of any memory leaks
    lang.CleanupPatterns();
    return;
  }

  Parse* pParser = Parse::Instance(root);
  bnf::UINT uCurrPos = 0;
  bool errorWritten = false;

  log << "\n=== Start parsing Input ===" << endl;
  while ((iSize = pParser->NextToken(input.substr(uCurrPos), tokens, iVariant)) != -1) {
    if (!tokens.empty()) {
      log << "*** TOKENS: ";
      for (auto& tkn : tokens) {
        log << " \"" << tkn << "\"";
      }
      log << endl;
    }
    if (iSize > 0) {
      log << "Parsed " << tokens.size() << " variant " << iVariant[1] << " size=" << iSize
          << " Line: " << input.substr(uCurrPos, iSize) << endl;
      tokens.clear();
    } else {
      //  text = "Parse returned 0\r\n";
      if (input.substr(uCurrPos, 1) == "\n") {
        errorWritten = false;
      } else if (!errorWritten) {
        log << "Error parsing at "
            << input.substr(uCurrPos, input.find("\n", uCurrPos) - uCurrPos) << endl;
        errorWritten = true;
      }
      iSize = 1;
    }
    uCurrPos += iSize;
  }
  log << "=== End of Input ===" << endl;
  lang.CleanupPatterns();
}

void BnfDocument::OnTextChange(wxCommandEvent& event) {
  Modify(true);
  event.Skip();
}
