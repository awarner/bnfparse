/*
 *  BNF Parser (bnfparse)
 *  Copyright © 2023 Andy Warner
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "bnfview.h"
#include "custom_events.h"
#include "mainpanel.h"
#include "wxmain.h"
#include "wx/wx.h"
using std::endl;

wxIMPLEMENT_DYNAMIC_CLASS(BnfView, wxView);

wxBEGIN_EVENT_TABLE(BnfView, wxView)
    EVT_MENU(wxID_COPY, BnfView::OnCopy)
    EVT_MENU(wxID_PASTE, BnfView::OnPaste)
    EVT_MENU(wxID_SELECTALL, BnfView::OnSelectAll)
wxEND_EVENT_TABLE()

BnfView::BnfView() {}

bool BnfView::OnCreate(wxDocument *doc, long flags) {
  if (!wxView::OnCreate(doc, flags)) {
    return false;
  }
  wxFrame* frame = wxGetApp().GetChildFrame(this);
  SetFrame(frame);
  wxASSERT(frame == GetFrame());
  wxGetApp().SetMenubar(true);
  panel_ = new MainPanel(frame, 0, 0, 1000, 900);
  panel_->SetMinSize(wxSize(1000, 900));
  panel_->Layout();
  //frame->Fit();
  // panel_->Layout();
  // panel_->GetSizer()->Fit(frame);
  frame->Show();

  return true;
}

void BnfView::OnDraw(wxDC *dc) {
  // wxTextCtrl takes care of drawing
}

bool BnfView::OnClose(bool deleteWindow) {
  if (!wxView::OnClose(deleteWindow)) {
    return false;
  }
  Activate(false);
  wxGetApp().SetMenubar(false);
  if (deleteWindow) {
    panel_->Hide();
  }
  // panel_->GetText("bnf")->Clear();
  // panel_->GetText("sample")->Clear();
  // panel_->GetText("root")->Clear();
  return true;
}

wxButton* BnfView::GetButton(int id) const {
  auto wnd = wxWindow::FindWindowById(ID_PARSE, panel_);
  if (wnd != nullptr) {
    GetLog() << "Got window for id " << id << endl;
    return static_cast<wxButton*>(wnd);
  }
  return nullptr;
}

std::ostream& BnfView::GetLog() const {
  return panel_->GetLog();
}

wxTextCtrl* BnfView::GetText(const std::string& name) const {
  return panel_->GetText(name);
}
