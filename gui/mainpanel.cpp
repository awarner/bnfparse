/*
 *  BNF Parser (bnfparse)
 *  Copyright © 2023 Andy Warner
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "custom_events.h"
#include "mainpanel.h"

MainPanel::MainPanel(wxFrame *frame, int x, int y, int w, int h)
    : wxPanel(frame, wxID_ANY, wxDefaultPosition, wxSize(w, h))
    , log_(nullptr) {
  auto rootSizer = new wxBoxSizer(wxHORIZONTAL);
  rootSizer->Add(new wxStaticText(this, wxID_ANY, "Root: "), 0, wxALIGN_CENTER | wxLEFT, 16);
  root_ = new wxTextCtrl(this, wxID_ANY, "",
                         wxDefaultPosition, wxSize(160, 24), wxHSCROLL);
  rootSizer->Add(root_, 2, wxALIGN_CENTER, 0);

  rootSizer->Add(new wxStaticText(this, wxID_ANY, "Sample file: "), 0, wxALIGN_CENTER | wxLEFT, 8);
  sample_filename_ = new wxTextCtrl(this, wxID_ANY, "",
                                    wxDefaultPosition, wxSize(140, 24), wxHSCROLL);
  rootSizer->Add(sample_filename_, 2, wxALIGN_CENTER, 0);

  auto browse = new wxButton(this, ID_BROWSE, "&Browse...",
                             wxDefaultPosition, wxDefaultSize);
  auto update = new wxButton(this, ID_PARSE, "&Update",
                             wxDefaultPosition, wxDefaultSize);
  rootSizer->Add(browse, 1, wxLEFT, 4);
  rootSizer->Add(update, 1, wxLEFT, 12);

  auto sampSizer = new wxBoxSizer(wxHORIZONTAL);
  sampSizer->Add(new wxStaticText(this, wxID_ANY, "Sample: "), 0, wxLEFT, 2);
  sample_ = new wxTextCtrl(this, wxID_ANY, "",
                           wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE | wxHSCROLL);
  sampSizer->Add(sample_, 1, wxEXPAND, 0);

  auto bnfSizer = new wxBoxSizer(wxHORIZONTAL);
  bnfSizer->Add(new wxStaticText(this, wxID_ANY, "BNF: "), 0, wxLEFT, 24);
  bnf_ = new wxTextCtrl(this, wxID_ANY, "",
                        wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE | wxHSCROLL);
  bnfSizer->Add(bnf_, 1, wxEXPAND, 0);

  auto outSizer = new wxBoxSizer(wxHORIZONTAL);
  outSizer->Add(new wxStaticText(this, wxID_ANY, "Output: "), 0, wxLEFT, 4);
  output_ = new wxTextCtrl(this, wxID_ANY, "",
                           wxDefaultPosition, wxDefaultSize,
                           wxTE_MULTILINE | wxHSCROLL | wxTE_READONLY);
  outSizer->Add(output_, 1, wxEXPAND, 0);

  auto vsizer = new wxBoxSizer(wxVERTICAL);
  vsizer->Add(rootSizer, 0, wxBOTTOM, 4);
  vsizer->Add(sampSizer, 2, wxEXPAND | wxBOTTOM | wxRIGHT, 4);
  vsizer->Add(bnfSizer, 3, wxEXPAND | wxBOTTOM | wxRIGHT, 4);
  vsizer->Add(outSizer, 2, wxEXPAND | wxRIGHT, 4);
  vsizer->SetSizeHints(this);
  SetSizer(vsizer);

  log_.rdbuf(output_);
}

std::ostream& MainPanel::GetLog() {
  return log_;
}

wxTextCtrl* MainPanel::GetText(const std::string& name) {
  if (name.empty() || name == "bnf") {
    return bnf_;
  } else if (name == "root") {
    return root_;
  } else if (name == "sample") {
    return sample_;
  } else if (name == "sample_filename") {
    return sample_filename_;
  } else if (name == "output") {
    return output_;
  }
  return nullptr;
}

void MainPanel::ClearOutput() {
  output_->Clear();
}
