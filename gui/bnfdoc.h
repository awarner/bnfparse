/*
 *  BNF Parser (bnfparse)
 *  Copyright © 2023 Andy Warner
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#pragma once

#include "wx/wx.h"
#include "wx/docview.h"

class BnfDocument : public wxDocument {
public:
  BnfDocument() = default;
  virtual ~BnfDocument() = default;

  virtual bool OnCreate(const wxString& path, long flags) override;

  wxButton* GetButton(int id);
  virtual wxTextCtrl* GetTextCtrl(const std::string& name = std::string()) const;

  virtual bool IsModified() const override;
  virtual void Modify(bool mod) override;

protected:
  virtual bool DoSaveDocument(const wxString& filename) override;
  virtual bool DoOpenDocument(const wxString& filename) override;

  void OnBrowseSample(wxCommandEvent& event);
  void OnParse(wxCommandEvent& event);
  void OnTextChange(wxCommandEvent& event);

  wxDECLARE_NO_COPY_CLASS(BnfDocument);
  wxDECLARE_EVENT_TABLE();
  wxDECLARE_DYNAMIC_CLASS(BnfDocument);
};
