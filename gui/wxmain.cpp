/*
 *  BNF Parser (bnfparse)
 *  Copyright © 2023 Andy Warner
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "bnfparse-config.h"
#include "bnfdoc.h"
#include "bnfview.h"
#include "custom_events.h"
#include "wxmain.h"

#include <wx/config.h>
#include <wx/docview.h>
#include <wx/wx.h>

// Forward reference
class MainPanel;

// This defines the equivalent of main() for the current platform.
wxIMPLEMENT_APP(BnfGui);

class MainFrame : public wxFrame {
public:
  MainFrame();
 
private:
  void OnExit(wxCommandEvent& event);

  MainPanel* panel_;

  // wxDECLARE_EVENT_TABLE();
};

///  MAIN APP

BnfGui::BnfGui()
    : frame_(nullptr)
    , fileMenuInit_(nullptr)
    , fileMenuOpened_(nullptr) {}


bool BnfGui::OnInit() {
  if (!wxApp::OnInit()) {
    return false;
  }

  ::wxInitAllImageHandlers();

  // Fill in the application information fields before creating wxConfig.
  SetVendorName("Andy Warner");
  SetAppName("bnfgui");
  SetAppDisplayName("BNF parser and viewer");

  //// Create a document manager
  wxDocManager *docManager = new wxDocManager;

  //// Create a template relating drawing documents to their views
  new wxDocTemplate(docManager, "BNF", "*.bnf", "", "bnf",
                    "BNF document", "BNF View",
                    CLASSINFO(BnfDocument), CLASSINFO(BnfView));
  docManager->SetMaxDocsOpen(1);
  frame_ = new wxDocParentFrame(docManager, nullptr, wxID_ANY,
                                GetAppDisplayName(),
                                wxDefaultPosition,
                                wxSize(1000, 900));

  auto menubar = CreateMenubar();
  frame_->SetMenuBar(menubar);
  frame_->CreateStatusBar();
  frame_->SetStatusText("BNF Parser");

  auto fileHist = docManager->GetFileHistory();
  auto baseId = fileHist->GetBaseId();
  Bind(wxEVT_MENU, &BnfGui::OnAbout, this, wxID_ABOUT);

  //MainFrame *frame = new MainFrame();
  frame_->Centre();
  frame_->Show();
  return true;
}

int BnfGui::OnExit() {
  wxDocManager * const manager = wxDocManager::GetDocumentManager();
#if wxUSE_CONFIG
  manager->FileHistorySave(*wxConfig::Get());
#endif // wxUSE_CONFIG
  delete manager;

  return wxApp::OnExit();
}

wxFrame* BnfGui::GetChildFrame(wxView* view) {
  return frame_;
}

void BnfGui::SetMenubar(bool opened) {
  auto menubar = frame_->GetMenuBar();
  if (menubar != nullptr) {
    if (opened) {
      menubar->Replace(0, fileMenuOpened_, "&File");
    } else {
      menubar->Replace(0, fileMenuInit_, "&File");
    }
  }
}
  

wxMenuBar* BnfGui::CreateMenubar() {
  fileMenuInit_ = new wxMenu;
  fileMenuInit_->Append(wxID_NEW);
  fileMenuInit_->Append(wxID_OPEN);
  fileMenuInit_->AppendSeparator();
  //fileMenuInit_->Append();  P&rint Setup...
  //fileMenuInit_->AppendSeparator();
  fileMruMenu_ = new wxMenu;
  fileMenuInit_->Append(ID_FILE_MRU, "Recent Files", fileMruMenu_);
  fileMenuInit_->AppendSeparator();
  fileMenuInit_->Append(wxID_EXIT);

  fileMenuOpened_ = new wxMenu;
  fileMenuOpened_->Append(wxID_NEW);
  fileMenuOpened_->Append(wxID_OPEN);
  fileMenuOpened_->Append(wxID_CLOSE);
  fileMenuOpened_->Append(wxID_SAVE);
  fileMenuOpened_->Append(wxID_SAVEAS);
  fileMenuOpened_->AppendSeparator();
  fileMenuOpened_->Append(wxID_PRINT);
  fileMenuOpened_->Append(wxID_PREVIEW);
  //fileMenuOpened_->Append();  P&ring Setup...
  fileMenuOpened_->AppendSeparator();
  fileMenuOpened_->Append(wxID_EXIT);

  auto menuEdit = new wxMenu;
  menuEdit->Append(wxID_UNDO);
  menuEdit->AppendSeparator();
  menuEdit->Append(wxID_CUT);
  menuEdit->Append(wxID_COPY);
  menuEdit->Append(wxID_PASTE);

  auto menuView = new wxMenu;
  menuView->AppendCheckItem(ID_VIEW_TOOLBAR, "&Toolbar", "View toolbar");
  menuView->AppendCheckItem(ID_VIEW_STATUS_BAR, "&Status Bar", "View status bar");
  menuView->AppendCheckItem(ID_VIEW_OUTPUT, "&Output Window", "View output window");
  menuView->AppendSeparator();
  menuView->AppendCheckItem(ID_VIEW_GRAPHICS, "&Graphical view", "View BNF tree/graphs");

  wxMenu *menuHelp = new wxMenu;
  menuHelp->Append(ID_HELP_FINDER, "&Help Topics", "Browse help topics");
  menuHelp->AppendSeparator();
  menuHelp->Append(wxID_ABOUT);

  wxMenuBar *menuBar = new wxMenuBar;
  menuBar->Append(fileMenuInit_, "&File");
  menuBar->Append(menuEdit, "&Edit");
  menuBar->Append(menuView, "&View");
  menuBar->Append(menuHelp,  wxGetStockLabel(wxID_HELP));

  wxDocManager* const docManager = wxDocManager::GetDocumentManager();
  docManager->FileHistoryUseMenu(fileMruMenu_);
#if wxUSE_CONFIG
  docManager->FileHistoryLoad(*wxConfig::Get());
#endif // wxUSE_CONFIG

  return menuBar;
}

void BnfGui::OnAbout(wxCommandEvent& event) {
  std::string version = std::to_string(Bnfparse_VERSION_MAJOR) + "." +
      std::to_string(Bnfparse_VERSION_MINOR);
  wxMessageBox("This is BNF Parser (bnfgui) version " + version +
               "\nCopyright (C) 2023 Andy Warner",
               "About BNF Parser", wxOK | wxICON_INFORMATION);
}



MainFrame::MainFrame()
    : wxFrame(nullptr, wxID_ANY, "BNF Parser", wxDefaultPosition, wxSize(1000, 900)) {

  // SetMenuBar(menuBar);
  // CreateStatusBar();
  // SetStatusText("BNF Parser");

  Bind(wxEVT_MENU, &MainFrame::OnExit, this, wxID_EXIT);

  // panel_ = new MainPanel(this, 10, 10, 900, 1000);
  // panel_->GetSizer()->Fit(this);
  // SetClientSize(panel_->GetSize());
}

void MainFrame::OnExit(wxCommandEvent& event) {
  Close(true);
}
