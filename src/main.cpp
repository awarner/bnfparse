/*
 *  BNF Parser (bnfparse)
 *  Copyright © 2023 Andy Warner
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "bnfparse-config.h"
#include "bnflanguage.h"
#include "parse.h"
#include <fstream>
#include <iostream>
using bnf::Parse;
using std::cerr;
using std::cout;
using std::endl;


bool readFile(const std::string& file, std::string& contents) {
  std::ifstream ifs(file);
  if (!ifs.good()) {
    return false;
  }
  contents.assign( (std::istreambuf_iterator<char>(ifs)),
                   (std::istreambuf_iterator<char>()));
  return true;
}

int main(int argc, char* argv[]) {
  cout << "Bnfparse Version " << Bnfparse_VERSION_MAJOR << "."
       << Bnfparse_VERSION_MINOR << endl;
  if (argc != 4) {
    cerr << "ERROR: Must specify pattern file, sample to parse and root as arguments." << endl;
    return 1;
  }
  std::string pPatternText;
  if (!readFile(argv[1], pPatternText)) {
    cerr << "Unable to read pattern file" << endl;
    return 2;
  }
  std::string pInput;
  if (!readFile(argv[2], pInput)) {
    cerr << "Unable to read input file" << endl;
    return 3;
  }
  std::string root(argv[3]);

  for (int i = 0; i < 2; ++i) {
    bnf::BnfLanguage lang;
    if (!lang.ParsePatterns(pPatternText)) {
      cerr << "ERROR: Could not parse pattern text (rules)" << endl;
      return 4;
    }

    bnf::INT iSize;
    bnf::INT iVariant[10];
    std::deque<std::string> token;

    //
    // Parse the main text ...
    //
    if (Parse::Lookup(root) == nullptr) {
      cerr << "Root pattern does not exist" << endl;
      // Get rid of any memory leaks
      lang.CleanupPatterns();
      return 5;
    }

    Parse* pParser = Parse::Instance(root);
    bnf::UINT uCurrPos = 0;
    bool errorWritten = false;

    cout << "\n=== Start parsing Input ===" << endl;
    while ((iSize = pParser->NextToken(pInput.substr(uCurrPos), token, iVariant)) != -1) {
      if (!token.empty()) {
        cout << "*** TOKENS: ";
        for (auto& tkn : token) {
          cout << " \"" << tkn << "\"";
        }
        cout << endl;
      }
      if (iSize > 0) {
        cout << "Parsed " << token.size() << " variant " << iVariant[1] << " size=" << iSize
             << " Line: " << pInput.substr(uCurrPos, pInput.find("\n", uCurrPos) - uCurrPos) << endl;
        token.clear();
      } else {
        //  text = "Parse returned 0\r\n";
        if (pInput.substr(uCurrPos, 1) == "\n") {
          errorWritten = false;
        } else if (!errorWritten) {
          cout << "Error parsing at "
               << pInput.substr(uCurrPos, pInput.find("\n", uCurrPos) - uCurrPos) << endl;
          errorWritten = true;
        }
        iSize = 1;
      }
      uCurrPos += iSize;
    }

    cout << "=== End of Input ===" << endl;
    lang.CleanupPatterns();
  }

  return 0;
}
