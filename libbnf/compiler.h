/*
 *  BNF Parser (bnfparse)
 *  Copyright © 2023 Andy Warner
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
//
// compiler.h
//

#pragma once

#include "bnfparsecommon.h"
#include <deque>
#include <string>

namespace bnf {

// Forward references:
class CEdit;
class Parse;


class Compiler {
public:
  Compiler();
  virtual ~Compiler();

  virtual bool Callback(Parse* pParse, std::deque<std::string>& token, INT iVariant);

private:
  // Disable copy constructor and assignment operator
  Compiler(const Compiler&) = delete;
  Compiler& operator= (const Compiler&) = delete;
};


//
// Specific compiler type for the BNF pattern buffer
//
class CompilePattern : public Compiler {
public:
  CompilePattern(CEdit& rOutWindow);
  virtual ~CompilePattern();

  virtual bool Callback(Parse* pParse, std::deque<std::string>& token, INT iVariant);

private:
  // Disable copy constructor and assignment operator
  CompilePattern(const CompilePattern&) = delete;
  CompilePattern& operator= (const CompilePattern&) = delete;

  CEdit& m_rOutWindow;
};

}  // namespace bnf
