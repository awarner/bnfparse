/*
 *  BNF Parser (bnfparse)
 *  Copyright © 2023 Andy Warner
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
//
// bnfsyntax.cpp
//

#include "bnfstatement.h"
#include "bnfsyntax.h"
#include <assert.h>
#include <iostream>
using std::cout;
using std::endl;

namespace bnf {

// static member declarations
BnfSyntax* BnfSyntax::instance_ = nullptr;


BnfSyntax::BnfSyntax()
  : Parse("_bnfs", "<_bnfs>") {
  std::pair<PARSEMAP::iterator, bool> retCode;
  retCode = parseDictionary_.insert(PARSEMAP::value_type("_bnfs", this));
  if (!retCode.second) {
    std::cerr << "ERROR: Could not insert _bnfs into dictionary" << endl;
    assert(false);
  }
}

BnfSyntax::~BnfSyntax() {
  PARSEMAP::iterator iter = parseDictionary_.find("_bnfs");
  if (iter == parseDictionary_.end()) {
    std::cerr << "ERROR: Destroy() could not find _bnfs in dictionary" << endl;
  } else {
    parseDictionary_.erase(iter);
  }
  instance_ = nullptr;
}

//
//         /-- bnf
// bnfs --|--- '|'
//         \-- bnf bnfs
bool BnfSyntax::Initialize() {
  // <_bnfs> ::= <_bnf> | <_bnf> '|' <_bnfs> | <_bnf> <_bnfs>

  Parse* bnf = BnfStatement::Instance();

  bnfTree_.AddAnd(bnf);

#if 0
  bnfTree_.AddOr (bnf);
  bnfTree_.AddAnd(Parse::Instance("_|", "|", true));
  bnfTree_.AddAnd(BnfSyntax::Instance());
#else
  bnfTree_.AddOr(Parse::Instance("_|", "|", true));
#endif

  bnfTree_.AddOr (bnf);
  bnfTree_.AddAnd(BnfSyntax::Instance());

  return true;
}

// Instance -- implement this class as a singleton
BnfSyntax* BnfSyntax::Instance() {
  if (!instance_) {
    cout << "BnfSyntax::Instance first called" << endl;
    instance_ = new BnfSyntax;
    instance_->Initialize();
    Parse::FreezeDictionary();
  }
  return instance_;
}

}  // namespace bnf
