/*
 *  BNF Parser (bnfparse)
 *  Copyright © 2023 Andy Warner
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "bnflanguage.h"
#include "bnfstatement.h"
#include "bnfsyntax.h"
#include "parse.h"
#include <iostream>
using std::endl;

namespace bnf {

BnfLanguage::BnfLanguage()
    : log(std::cout) {}

BnfLanguage::BnfLanguage(std::ostream& os)
    : log(os) {}

BnfLanguage::~BnfLanguage() {}

void BnfLanguage::CleanupPatterns() {
  log << "Cleanup patterns" << endl;
  // for (auto parse : patterns_) {
  //   log << " - Destroy " << parse->GetName() << endl;
  //   parse->Destroy();
  // }
  log << "\nBefore clearing" << endl;
  Parse::PrintDictionary();
  Parse::ClearNonFrozen();
  patterns_.clear();

  //delete BnfSyntax::Instance();
  //delete BnfStatement::Instance();
  log << "\nAfter clearing" << endl;
  Parse::PrintDictionary();
}

const std::vector<Parse*>& BnfLanguage::GetPatterns() {
  return patterns_;
}

bool BnfLanguage::ParsePatterns(const std::string& pPatternText) {
  patterns_.clear();

  auto comment = Parse::Lookup("_comment");
  if (comment == nullptr) {
    comment = Parse::Instance("_comment", "'/*' R'(.|\\n)*?\\*/' | '//' R'.*'");
    if (!comment->Initialize()) {
      log << "Comment pattern did not initialize" << endl;
    }
  }
  comment->Freeze(false);
  patterns_.push_back(comment);

  auto identifier = Parse::Lookup("_identifier");
  if (identifier == nullptr) {
    identifier = Parse::Instance("_identifier", "'<' R'[a-zA-Z0-9_]+' '>'");
    if (!identifier->Initialize()) {
      log << "Identifier pattern did not initialize" << endl;
    }
  }
  patterns_.push_back(identifier);

  auto bnfDef = Parse::Lookup("_definition");
  if (bnfDef == nullptr) {
    bnfDef = Parse::Instance("_definition",
                             "<_identifier> ::= R'.*' | '|' R'.*' | <_comment>");
    if (!bnfDef->Initialize()) {
      log << "Definition pattern did not initialize" << endl;
    }
  }
  patterns_.push_back(bnfDef);
  log << "BNF patterns initialized.\n" << endl;

  INT iSize;
  INT iVariant[10];
  std::deque<std::string> token;
  Parse* pParse;
  bool bCanContinue = false;
  std::string ident;
  std::string pattern;

  INT iCurrPos = 0;
  while ((iSize = bnfDef->NextToken(pPatternText.substr(iCurrPos), token, iVariant)) != -1) {
    if (iSize > 0) {
      log << "Called _definition variant " << iVariant[1] << endl;
      std::string scanned(pPatternText, iCurrPos, iSize);
      log << "Got " << token.size() << " tokens, size " << iSize << ": " << scanned << endl;

      std::deque<std::string>::iterator itToken;
      for (itToken = token.begin(); itToken != token.end(); ++itToken) {
        log << "-> " <<  (*itToken) << endl;
      }

      if (bCanContinue && iVariant[1] != 2) {
        bCanContinue = false;
        pParse = Parse::Instance(ident, pattern);
        if (!pParse->Initialize()) {
          log << "Parse error for variable" << endl;
        }
        patterns_.push_back(pParse);
      }

      switch (iVariant[1]) {
        case 1:		// bnf definition found
          ident = token[1];
          pattern = token[4];
          bCanContinue = true;
          break;
        case 2:
          pattern += " | " + token[1];
          break;
        case 3:   // comment
          bCanContinue = false;
          break;
        default:
          log << "Invalid variant: " << iVariant[1] << endl;
      }

      token.clear();
      log << "+=+ end +=+" << endl;
    } else {
//y			text = "Parse returned 0\r\n";
      if (bCanContinue) {
        pParse = Parse::Instance(ident, pattern);
        bCanContinue = false;
        if (!pParse->Initialize()) {
          log << "Parse error for variable" << endl;
        }
        patterns_.push_back(pParse);
      }
      iSize = 1;
    }

    iCurrPos += iSize;
  }

  if (bCanContinue) {  // Process the last line of input file
    pParse = Parse::Instance(ident, pattern);
    if (!pParse->Initialize()) {
      log << "Parse error for variable: " << ident << endl;
    }
    patterns_.push_back(pParse);
  }

  log << "=== End of Pattern Input ===" << endl;
  return true;
}

}  // namespace bnf
