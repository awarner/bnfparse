/*
 *  BNF Parser (bnfparse)
 *  Copyright © 2023 Andy Warner
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#pragma once

#include "bnfparsecommon.h"
#include <deque>
#include <list>
#include <string>

namespace bnf {
//
// Forward references:
//
class Parse;

//
// Type Definitions:
//
using PARSELIST = std::list<Parse*>;


class BnfTree {
public:
  BnfTree();
  ~BnfTree();

  bool AddAnd(Parse* pParse);
  bool AddOr (Parse* pParse);
  void Clear();
  /**
   *  Parse in the input buffer against the BNF tree.  For each 
   *  "terminating" node of the tree, push the matching input token 
   *  onto the token stack.
   *
   *  As precondition, the BNF tree has been setup by calling Initialize.
   *
   *  > Note that this function along with Parse::NextToken are recursive.
   *  @param input  The pattern sequence to be parsed
   *  @param tokens Stack of parsed tokens
   *  @param iVariant  Which variant of a BNF caused the match
   *  @return A 0 is returned if the input did not match, a -1 is returned
   *          on End-Of-File. Otherwise, the total size matched from the input
   *          buffer is returned.
   */
  INT  DoParse(const std::string& input, std::deque<std::string>& tokens, INT iVariant[]);
  bool IsEmpty() const;

  void PrintTree();
  INT  SkipWhiteSpace(const std::string& input);
  void TrimWhiteSpace(std::string& str, bool replace_special = true);

private:
  // Disable copy constructor and assignment operator
  BnfTree(const BnfTree&) = delete;
  BnfTree& operator= (const BnfTree&) = delete;

  friend class Parse;

  void TokenClear(std::deque<std::string>& token, char cWho);
  void TokenPush(std::deque<std::string>& token, const std::string& str, char cWho);

public:	 // need to reference this from BnfGraphView
  std::list<PARSELIST*> parseList_;
};

}  // namespace bnf
