/*
 *  BNF Parser (bnfparse)
 *  Copyright © 2023 Andy Warner
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
//
// parse.h
//
#pragma once

#include "bnfparsecommon.h"
#include "bnftree.h"

#include <deque>
#include <map>
#include <regex>
#include <string>

namespace bnf {
//
// Forward references:
//
class Compiler;
class GraphicInfo;


class Parse {
public:
  using PARSEMAP = std::map<std::string, Parse*>;

  void Destroy();
//  void EraseDictionary();
  const std::string& GetBnf() const;
  BnfTree* GetBnfTree();
  const std::string& GetName() const;
  GraphicInfo* Graphics();
  INT RegexpCmp(const std::string& pInput, const std::string& pPattern, bool bLiteral);
  INT RegExec(std::regex* regInput, const std::string& pInput);
  void SetCallback(Compiler* pCompiler);
  void SetLiteral();

  void Freeze(bool doFreeze);
  static void FreezeDictionary();
  bool IsFrozen() const;

  /** Return the singleton instance of a parsing pattern, determined 
   *  by the "pName" parameter.  A new instance is created if pName 
   *  was not already defined in the global dictionary, "m_ParseDictionary."
   *
   *  Externally, this function must always be used instead of the constructor.
   *  In fact, the constructor is private to enforce this restriction.
   *
   *  @param name Name of the pattern.  This will be the key into the global dictionary
   *  @param bnf  Pattern definition (default = "")
   *  @param literal Set up the pattern for literal (true) or regular-expression (false)
   *              matching. (default = false)
   */
  static Parse* Instance(const std::string& name, const std::string& bnf = "", bool literal = false);
  static Parse* Lookup(const std::string& pName);
  static void ClearNonFrozen();
  static void PrintDictionary();
  virtual bool Initialize();
  /**
   *  Get the next tokens that match the BNF
   *  @param input  The input string to parse
   *  @param token  Array of tokens that were parsed from the input
   *  @param iVariant Array of variant numbers that caused the cooresponding token to be matched
   *
   *  @return  >0  size of input pattern, 0  did not match, -1  end of input
   */
  virtual INT NextToken(const std::string& input, std::deque<std::string>& token, INT iVariant[]);

protected:
  BnfTree bnfTree_;
  // Only called from Instance() and subclasses
  Parse(const std::string& name, const std::string& bnf);
  // Only called from Destroy() and subclasses
  virtual ~Parse();

  static PARSEMAP parseDictionary_;

private:
  // Disable copy constructor and assignment operator
  Parse(const Parse&) = delete;
  Parse& operator= (const Parse&) = delete;

  PARSEMAP& GetDictionary();
  Parse* InitPattern(std::string& token, bool literal);

  std::string name_;
  std::string bnf_;
  bool literal_;
  Compiler* compiler_;
  GraphicInfo* graphicInfo_;
  UINT refCount_;  // Reference count for garbage collection

  std::regex regInput_;
  INT compiled_;
  bool frozen_;

public:
  static UINT depth_;  // Depth of recursion
};

}  // namespace bnf
