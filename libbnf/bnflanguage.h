/*
 *  BNF Parser (bnfparse)
 *  Copyright © 2023 Andy Warner
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#pragma once

#include <ostream>
#include <string>
#include <vector>

namespace bnf {

// Forward references
class Parse;


class BnfLanguage {
public:
  BnfLanguage();
  BnfLanguage(std::ostream& os);
  ~BnfLanguage();

  void CleanupPatterns();
  const std::vector<Parse*>& GetPatterns();
  bool ParsePatterns(const std::string& pPatternText);

private:
  std::ostream& log;
  std::vector<Parse*> patterns_;
};

}  // namespace bnf
