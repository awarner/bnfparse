/*
 *  BNF Parser (bnfparse)
 *  Copyright © 2023 Andy Warner
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "compiler.h"
#include "bnfsyntax.h"
#include "bnftree.h"
#include "graphicinfo.h"
#include <iostream>
using std::cout;
using std::endl;

namespace bnf {

BnfTree::BnfTree() {}

BnfTree::~BnfTree() {
  std::list<PARSELIST*>::iterator idxArray;
  std::list<Parse*>::iterator idxParse;
  for (idxArray  = parseList_.begin();
       idxArray != parseList_.end();
       idxArray++) {
    for (idxParse  = (*idxArray)->begin();
         idxParse != (*idxArray)->end();
         idxParse++) {
      //D (*idxParse)->Destroy();
    }
    (*idxArray)->clear();
    delete *idxArray;
  }
  parseList_.clear();
}

bool BnfTree::AddAnd(Parse* pParse) {
  if (parseList_.empty()) {  // this statement allows starting a BNF with "and"
    parseList_.push_back((new PARSELIST));
  }
  std::list<PARSELIST*>::reverse_iterator last;
  last = parseList_.rbegin();
  (*last)->push_back(pParse);

  return true;
}

bool BnfTree::AddOr(Parse* pParse) {
  parseList_.push_back((new PARSELIST));
  std::list<PARSELIST*>::reverse_iterator last;
  last = parseList_.rbegin();
  (*last)->push_back(pParse);

  return true;
}

void BnfTree::Clear() {
  parseList_.clear();
}

INT BnfTree::DoParse(const std::string& pInput, std::deque<std::string>& token, INT iVariant[]) {
  INT iReturn = -1;

  std::list<PARSELIST*>::iterator itOr;
  for (itOr = parseList_.begin(); itOr != parseList_.end(); ++itOr, ++iVariant[Parse::depth_]) {
    std::string pScan;
    INT iSize = 0;
    INT iWhiteSpace = 0;
    PARSELIST::iterator itAnd;

    for (itAnd = (*itOr)->begin(); itAnd != (*itOr)->end(); ++itAnd) {
      pScan = pInput.substr(iSize);
      iReturn = (*itAnd)->NextToken(pScan, token, iVariant);
      if (iReturn < 1) {
        // TODO: check { optional } here
        TokenClear(token, 'b');
        break;
      }
      auto ws = SkipWhiteSpace(pScan.substr(iReturn));
      iWhiteSpace += ws;
      iSize += iReturn + ws;
    }
    if (iReturn > 0) {  // Found a match
      return iSize;
    }
  }

  TokenClear(token, 'B');
  if (iReturn == -1) iReturn = 0;
  return iReturn;
}

bool BnfTree::IsEmpty() const {
  return parseList_.empty();
}

void BnfTree::PrintTree() {
  std::list<PARSELIST*>::iterator itOr;
  for (itOr = parseList_.begin(); itOr != parseList_.end(); ++itOr) {
    if (itOr != parseList_.begin()) {
      cout << " OR ";
    }
    PARSELIST::iterator itAnd;
    for (itAnd = (*itOr)->begin(); itAnd != (*itOr)->end(); ++itAnd) {
      if (itAnd != (*itOr)->begin()) {
        cout << " AND ";
      }
      cout << (*itAnd)->GetBnf();
    }
  }
  cout << endl;
}

INT BnfTree::SkipWhiteSpace(const std::string& input) {
  if (input.empty()) {
    return 0;
  }
  auto pos = input.find_first_not_of(" \t\n\r");
  if (pos == std::string::npos) {  // End of parsing
    pos = 0;
  }
  return pos;
}

void BnfTree::TokenClear(std::deque<std::string>& token, char cWho) {
  if (!token.empty()) {
    //y		TRACE("-~-~%c %d CLEARED\n", cWho, Parse::m_uDepth);
    token.clear();
  }
}

void BnfTree::TokenPush(std::deque<std::string>& token, const std::string& str, char cWho) {
//y	TRACE("-~-~%c %d %d %s\n", cWho, Parse::m_uDepth, iSize, strPushed.c_str());
  token.push_back(str);
}

void BnfTree::TrimWhiteSpace(std::string& str, bool replace_special) {
  if (str.empty()) {
    return;
  }

  INT iChar = str.find_last_not_of(" \t\n");
  str.erase(iChar + 1);

  if (!replace_special) {
    return;
  }

  // Now, replace escaped special characters
  INT iPos = 0;
  const char* chTo;
  while ((iChar = str.find_first_of("\\", iPos)) != -1) {
    switch (str[iChar + 1]) {
      case 'n':
        chTo = "\n";
        break;
      case 'r':
        chTo = "\r";
        break;
      case 't':
        chTo = "\t";
        break;
      default:
        chTo = "\0";
    }

    if (*chTo != '\0') {
      str.replace(iChar, 2, chTo);
    }
    iPos = iChar + 1;
  }
}

}  // namespace bnf
