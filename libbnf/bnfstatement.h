/*
 *  BNF Parser (bnfparse)
 *  Copyright © 2023 Andy Warner
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
//
// bnfstatement.h
//

#pragma once

#include "parse.h"

namespace bnf {

class Regexp : public Parse {
public:
  Regexp();
  ~Regexp();

  bool Initialize() override;
};


class BnfStatement : public Parse {
public:
  virtual ~BnfStatement();

  bool Initialize() override;

  static BnfStatement* Instance();

private:
  // Disable copy constructor and assignment operator
  BnfStatement(const BnfStatement&) = delete;
  BnfStatement& operator= (const BnfStatement&) = delete;
  // Constructor only allowed thru Instance() method
  BnfStatement();

  Regexp* regexp_;

  static BnfStatement* instance_;
};

}  // namespace bnf
