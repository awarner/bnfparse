/*
 *  BNF Parser (bnfparse)
 *  Copyright © 2023 Andy Warner
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
//
// bnfsyntax.h
//

#pragma once

#include "parse.h"

namespace bnf {

class BnfSyntax : public Parse {
public:
  virtual ~BnfSyntax();

  bool Initialize() override;

  static BnfSyntax* Instance();

private:
  // Disable copy constructor and assignment operator
  BnfSyntax(const BnfSyntax&) = delete;
  BnfSyntax& operator= (const BnfSyntax&) = delete;
  // Constructor only allowed thru Instance() method
  BnfSyntax();

  static BnfSyntax* instance_;
};

}  // namespace bnf
