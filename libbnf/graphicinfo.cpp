/*
 *  BNF Parser (bnfparse)
 *  Copyright © 2023 Andy Warner
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
//
// graphicinfo.cpp -- Size, font, line, etc. needed to render a parsing object
//

#include "graphicinfo.h"

namespace bnf {

GraphicInfo::GraphicInfo() {}

GraphicInfo::~GraphicInfo() {}

CRect& GraphicInfo::GetRect() {
  return rect_;
}

void GraphicInfo::SetRect(const CRect& rect) {
  rect_ = rect;
}

}  // namespace bnf
