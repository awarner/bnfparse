/*
 *  BNF Parser (bnfparse)
 *  Copyright © 2023 Andy Warner
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "bnfsyntax.h"
#include "compiler.h"
#include "graphicinfo.h"
#include "parse.h"
#include <assert.h>
#include <iostream>
using std::cout;
using std::cerr;
using std::endl;

namespace bnf {

// static member declarations
Parse::PARSEMAP Parse::parseDictionary_;
UINT Parse::depth_ = 0;


Parse::Parse(const std::string& name, const std::string& bnf)
  : name_(name)
  , bnf_(bnf)
  , literal_(false)
  , compiler_(nullptr)
  , graphicInfo_(nullptr)
  , refCount_(0)
  , compiled_(0)
  , frozen_(false) {}

Parse::~Parse() {
  cout << "TRACE: Parse destructor for " << name_ << endl;
  if (graphicInfo_) {
    delete graphicInfo_;
  }
}

void Parse::Destroy() {
  if (!frozen_ && --refCount_ == 0) {
    cout << "TRACE: Parse Destroy for " << name_ << endl;
    PARSEMAP::iterator iter = parseDictionary_.find(name_);
    if (iter == parseDictionary_.end()) {
      cerr << "ERROR: Destroy() could not find " << name_ << " in dictionary" << endl;
    } else {
      parseDictionary_.erase(iter);
    }
    delete this;
  }
}

void Parse::ClearNonFrozen() {
  std::vector<std::string> delkeys;
  for (auto& [key, parse] : parseDictionary_) {
    if (!parse->IsFrozen()) {
      delkeys.push_back(key);
      //delete parse;
    }
  }
  for (const auto& key : delkeys) {
    auto it = parseDictionary_.find(key);
    if (it != parseDictionary_.end()) {
      parseDictionary_.erase(it);
      delete (*it).second;
    }
  }
}

#if 0
void Parse::EraseDictionary() {
  PARSEMAP::iterator itMap;
  for (itMap = m_ParseDictionary.begin(); itMap != m_ParseDictionary.end(); ++itMap)
  {
    delete (*itMap).second;
  }
  m_ParseDictionary.clear();
}
#endif

void Parse::Freeze(bool doFreeze) {
  frozen_ = doFreeze;
}

void Parse::FreezeDictionary() {
  for (auto& [key, parse] : parseDictionary_) {
    parse->Freeze(true);
  }
}

bool Parse::IsFrozen() const {
  return frozen_;
}

Parse::PARSEMAP& Parse::GetDictionary() {
  return parseDictionary_;
}

Parse* Parse::Instance(const std::string& name, const std::string& bnf, bool literal) {
  Parse* pParse;
  PARSEMAP::iterator iter = parseDictionary_.find(name);
  if (iter == parseDictionary_.end()) {
    cout << "TRACE: new Parse " << name << endl;
    pParse = new Parse(name, bnf);
    if (literal) {
      pParse->SetLiteral();
    }
    auto retCode = parseDictionary_.insert(PARSEMAP::value_type(name, pParse));
    if (!retCode.second) {
      cerr << "ERROR: Could not insert " << name << " into dictionary" << endl;
      assert(false);
    }
  } else {
    pParse = (*iter).second;
    if (literal != pParse->literal_) {
      // This pattern already used in another way!
      cerr << "WARNING: " << pParse->GetName() << " redefined as "
           << (literal ? "literal" : "non-literal") << endl;
      pParse->literal_ = literal;
      if (!bnf.empty()) {
        pParse->bnf_ = bnf;
      }
    }
  }

  pParse->refCount_++;
  return pParse;
}

bool Parse::Initialize() {
  INT iMid = 0;
  INT iSize;
  INT iVariant[10];
  bool bNextOr = false;
  std::deque<std::string> token;
  Parse* pBnf = BnfSyntax::Instance();

  cout << "=== Initialize the BNF ===" << endl;
  bnfTree_.Clear();
  while ((iSize = pBnf->NextToken(bnf_.substr(iMid), token, iVariant)) != -1) {
    if (iSize > 0) {
      bool replace = true;
      if (iVariant[1] == 1 && iVariant[2] == 3) {  // Strip off R' and ' from regex
        if (bnf_[iMid + iSize - 1] == ' ') {
          --iSize;
        }
        bnf_.erase(iMid + iSize - 1, 1);
        bnf_.erase(iMid, 2);
        iSize -= 3;
        replace = false;
      }
      std::string scanned(bnf_, iMid, iSize);
      bnfTree_.TrimWhiteSpace(scanned, replace);
      cout << "Called " << pBnf->GetName() << " variants " << iVariant[1] << ", " << iVariant[2]
           << ", token " << scanned << " tokens=" << token.size() << endl;

      bnfTree_.TrimWhiteSpace(token.back(), replace);
      if (token.back() == "|") {
        bNextOr = true;		// The next statement will be OR'd
      } else {
        Parse* pPattern = InitPattern(scanned, replace);
        bNextOr ? bnfTree_.AddOr(pPattern) : bnfTree_.AddAnd(pPattern);
        bNextOr = false;
      }
    } else {
      cout << "BNF parse returned 0" << endl;
      iSize = 1;
    }
    iMid += iSize;
    token.clear();
  }

  cout << ">>> End of BNF --- My BNF tree is:" << endl;
  bnfTree_.PrintTree();
  cout << "<<< End of BnfSyntax Tree" << endl;

  return true;
}

#if 1
Parse* Parse::InitPattern(std::string& token, bool literal) {
  Parse* pPattern;
  char chLast = token.back();

  // Reference to another pattern definition
  std::string pattName;
  if (token[0] == '<' && chLast == '>') {
    pattName = token.substr(1, token.size() - 2);
  } else {
    pattName = token;
  }
  pPattern = Parse::Lookup(pattName);
  if (pPattern == nullptr) {
    if (token[0] == '\'' && chLast == '\'') {
      pPattern = Parse::Instance(pattName, token.substr(1, token.size() - 2), literal);
    } else {
      pPattern = Parse::Instance(pattName, token, literal);
    }
  }
  return pPattern;
}
#else
Parse* Parse::InitPattern(std::string& token, bool literal) {
  Parse* pPattern;
  char chLast = token.back();

  // Reference to another pattern definition
  if (token[0] == '<' && chLast == '>') {
    pPattern = Parse::Instance(token.substr(1, token.size() - 2));
  } else if (token[0] == '\'' && chLast == '\'') {
    // if (strcmp(pBnf->GetName(), "_bnf") == 0 && iVariant == 2)
      pPattern = Parse::Instance(token, token.substr(1, token.size() - 2));
  } else {
    pPattern = Parse::Instance(token, token);
  }
  if (literal) {
    pPattern->SetLiteral();
  }
  return pPattern;
}
#endif

Parse* Parse::Lookup (const std::string& name) {
  Parse* pParse = nullptr;
  PARSEMAP::iterator iter = parseDictionary_.find(name);
  if (iter != parseDictionary_.end()) {
    pParse = (*iter).second;
  }
  return pParse;
}

INT Parse::NextToken(const std::string& input, std::deque<std::string>& token, INT iVariant[]) {
  if (input.empty()) {  // End of input -- get outta here!
    return -1;
  }

  INT iSize;
  if (bnfTree_.IsEmpty()) {  // This pattern is a terminal
    if ((iSize = RegexpCmp(input, bnf_, literal_)) <= 0) {
      bnfTree_.TokenClear(token, 'p');
      return 0;
    }

    bnfTree_.TokenPush(token, std::string(input, 0, iSize), 'p');
  } else {
    // non-terminal: walk the parse tree
    ++depth_;		// Record depth of recursion
    iVariant[depth_] = 1;
    iSize = bnfTree_.DoParse(input, token, iVariant);
  }

  if (compiler_ && iSize > 0) {
    compiler_->Callback(this, token, iVariant[depth_]);
  }

  if (! bnfTree_.IsEmpty()) {
    --depth_;
  }

  return iSize;
}

const std::string& Parse::GetBnf() const {
  return bnf_;
}

BnfTree* Parse::GetBnfTree() {
  return &bnfTree_;
}

const std::string& Parse::GetName() const {
  return name_;
}

GraphicInfo* Parse::Graphics() {
  if (graphicInfo_ == nullptr) {
    graphicInfo_ = new GraphicInfo;
  }
  return graphicInfo_;
}

void Parse::PrintDictionary() {
  cout << " * * PARSE  DICTIONARY * *" << endl;

  PARSEMAP::iterator itMap;
  for (itMap = parseDictionary_.begin(); itMap != parseDictionary_.end(); ++itMap) {
    Parse* pParse = (*itMap).second;
    cout << " " << pParse->refCount_ << " " << (pParse->frozen_ ? "F" : " ") << " "
         << pParse->GetName() << " ==> " << pParse->GetBnf() << endl;
  }
}

INT Parse::RegexpCmp(const std::string& pInput, const std::string& pPattern, bool literal) {
  INT iReturn = -1;

  if (literal == false && compiled_ == 0) {
    regInput_.assign(pPattern, std::regex_constants::optimize);
    compiled_ = 1;
  }

  if (literal || compiled_ != 1) {
    if (pInput.substr(0, pPattern.size()) == pPattern) {
      iReturn = pPattern.size();
    }
  } else {
    iReturn = RegExec(&regInput_, pInput);
  }

  return iReturn;
}

INT Parse::RegExec(std::regex* regInput, const std::string& input) {
  std::smatch sm;
  if (std::regex_search(input, sm, *regInput, std::regex_constants::match_continuous)) {
    return sm.length();
  }
  return -1;
}

void Parse::SetCallback(Compiler* compiler) {
  compiler_ = compiler;
}

void Parse::SetLiteral() {
  literal_ = true;
}

}  // namespace bnf
