/*
 *  BNF Parser (bnfparse)
 *  Copyright © 2023 Andy Warner
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
//
// bnfstatement.cpp
//

#include "bnfstatement.h"
#include "bnfsyntax.h"
#include <assert.h>
#include <iostream>
using std::cout;
using std::endl;

namespace bnf {

// static member declarations
BnfStatement* BnfStatement::instance_ = nullptr;


Regexp::Regexp()
  : Parse("_regexp", "<_regexp>") {
  Initialize();	
}

Regexp::~Regexp() {}

//
// regexp ------ R' ([^'\\]|\\.)* '
//
bool Regexp::Initialize() {
  bnfTree_.AddOr (Parse::Instance("_R'", "R'", true));
  bnfTree_.AddAnd(Parse::Instance("_([^'\\\\]|\\\\.)*", "([^'\\\\]|\\\\.)*"));
  bnfTree_.AddAnd(Parse::Instance("_'", "'", true));

  return true;
}


BnfStatement::BnfStatement()
  : Parse("_bnf", "<_bnf>") {
//	Initialize();
}

BnfStatement::~BnfStatement() {
  instance_ = nullptr;
  delete regexp_;
}

//
//        /-- < symbol >
//       |--- ' ^'* '
// bnf --|--- regex
//       |--- ^|{}' \t\n
//        \-- { bnfs }
bool BnfStatement::Initialize() {
  // <_bnfs> ::= <_bnf> | <_bnf> '|' <_bnf> | <_bnf> <_bnfs>

  // Escape characters
  //	"_escapes", "\\[bnt'\"\\]"

  bnfTree_.AddAnd(Parse::Instance("_<", "<", true));
  bnfTree_.AddAnd(Parse::Instance("_[a-zA-Z0-9_]+", "[a-zA-Z0-9_]+"));
  bnfTree_.AddAnd(Parse::Instance("_>", ">", true));

  bnfTree_.AddOr (Parse::Instance("_'", "'", true));
  bnfTree_.AddAnd(Parse::Instance("_[^\\']*", "[^\\']*"));
  bnfTree_.AddAnd(Parse::Instance("_'", "'", true));

  bnfTree_.AddOr (regexp_ = new Regexp());

  bnfTree_.AddOr (Parse::Instance("_terminal", "([^|{}\\' \\\\\\n\\r\\t]|\\\\.)+"));

  bnfTree_.AddOr (Parse::Instance("_{", "{", true));
  bnfTree_.AddAnd(BnfSyntax::Instance());
  bnfTree_.AddAnd(Parse::Instance("_}", "}", true));

  return true;
}

// Instance -- implement this class as a singleton
BnfStatement* BnfStatement::Instance() {
  if (!instance_) {
    instance_ = new BnfStatement;
    instance_->Initialize();
  }
  return instance_;
}

}  // namespace bnf
