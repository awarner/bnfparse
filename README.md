# bnfparse Parser for Backus-Naur Form in C++

Backus-Naur Form, or BNF for short, is a context-free grammar used to define the syntax
of computer languages and other context-free languages. It's simple syntax can be used
to describe the rules of grammar for a language. Basically, you use a set of rules that
are of the form "<non-terminal> ::= combinations of terminals and non-terminals".  For example,
"<abcrule> ::= abc | <abcrule> abc" meaning the non-terminal "<abc>" is defined by either the
letters "abc" or the non-terminal <abc> followed by the letters "abc".

This program is written in C++ and uses an extended BNF syntax where non-terminals are 
enclosed in angle-brackets and terminals can be expressed by regex.
A terminal expression is defined as a literal or a regex (regular expression) string for matching. Terminal expressions that are regex are enclosed by an "R" and followed by a simgle-quote. For example:

* 0 | 1   Expression matches either the numeral 0 or the numeral 1
* '0' | '1' Same as expression above. Terminals are optionally enclosed by single-quotes
* R'a.*g' Expression matches the regex of "a.*g' any word starting with "a" and ending in "g" such as "ag", "aag" or "anything".

> Note that this program uses C++ STL std::regex with the ECMAscript syntax (which is the default).

A rule is defined by a non-terminal name enclosed in angle brackets ("<", ">") followed by "::=" and then an expression. The expression is any series of terminals and non-terminals. A vertical bar ("|") is used to define variants, meaning the non-terminal can be defined by either alternative expression.

Here is a sample rule:

    <comment> ::= '/*' R'(.|\n)*?\*/' | '//' R'.*'



This code uses C++17 and does not rely on any external libraries. It uses cmake for building. If wxWidgets is installed on your system, then the program "bnfgui" will be built.

